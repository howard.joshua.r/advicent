using CollegeTuitionAPI;
using CollegeTuitionAPI.Controllers;
using CollegeTuitionAPI.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Xunit;

namespace CollegeTuitionAPITests
{
    public class TestRunner
    {
        CollegeController _controller;

        public TestRunner()
        {
            var builder = new DbContextOptionsBuilder<CollegeContext>().UseInMemoryDatabase("Colleges");
            CollegeContext _context = new CollegeContext(builder.Options);            
            _controller = new CollegeController(_context);            
        }   

        [Fact]
        public void ItCanHandleNoCollege()
        { 
            var result = _controller.GetColleges();
            Assert.Single(result);
            Assert.Equal("Error: College name is required!", result[0]);
        }

        [Fact]
        public void ItCanReturnAllColleges()
        {
            var result = _controller.GetCollegeTuition("all");
            Assert.True(result.Count > 100, "Less than 100 Colleges Returned");
        }

        [Fact]
        public void ItCanHandleIncorrectCollege()
        {
            var result = _controller.GetCollegeTuition("This Does Not Exist");
            Assert.Single(result);
            Assert.Equal("Error: College 'This Does Not Exist' Not Found! Please check the spelling and try again.", result[0]);
        }

        [Fact]
        public void ItCanRetrieveOneCollege()
        {
            var result = _controller.GetCollegeTuition("Adelphi University");
            Assert.Equal("$54,184.00", result[0]);
        }

        [Fact]
        public void ItCanRetrieveOneCollegeWithRoomAndBoard()
        {
            var result = _controller.GetCollegeTuition("Adelphi University", true);
            Assert.Equal("$54,184.00", result[0]);
        }

        [Fact]
        public void ItCanRetrieveOneCollegeWithOutRoomAndBoard()
        {
            var result = _controller.GetCollegeTuition("Adelphi University", false);
            Assert.Equal("$38,657.00", result[0]);
        }
    }
}
