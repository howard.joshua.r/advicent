﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CollegeTuitionAPI.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CollegeTuitionAPI.Controllers
{
    [Route("api/college")]
    [ApiController]
    public class CollegeController : ControllerBase
    {
        private readonly CollegeContext _context;

        public CollegeController(CollegeContext context)
        {
            _context = context;

            if (_context.Colleges.Count() == 0)
            {
                List<College> colleges = BuildCollegesList();
                int id = 1;
                foreach (var college in colleges)
                {
                    college.ID = id;
                    _context.Colleges.Add(college);
                    id++;
                }                
            }
        }

        // GET: api/<controller>
        [HttpGet]
        public List<string> GetColleges()
        {
            return new List<string> { "Error: College name is required!" };
        }

        // GET api/<controller>/5
        [HttpGet("{name}")]
        public List<string> GetCollegeTuition(string name)
        {
            if (name.ToLower() == "all")
            {
                List<College> colleges = _context.Colleges.Local.ToList();
                List<string> names = new List<string>();

                foreach (var college in colleges)
                {
                    names.Add(college.Name);
                }

                return names;
            }
            else
            {
                College college = null;
                foreach (var item in _context.Colleges.Local.ToList())
                {
                    if (item.Name == name)
                    {
                        college = item;
                        break;
                    }
                }

                if (college == null)
                    return new List<string> { "Error: College '" + name + "' Not Found! Please check the spelling and try again." };

                return new List<string> { string.Format("{0:C}", GetTuition(college, true)) };
            }
        }

        // GET api/<controller>/5
        [HttpGet("{name}/{roomAndBoard}")]
        public List<string> GetCollegeTuition(string name, bool roomAndBoard)
        {
            if (name.ToLower() == "all")
            {
                List<College> colleges = _context.Colleges.Local.ToList();
                List<string> names = new List<string>();

                foreach (var college in colleges)
                {
                    names.Add(college.Name);
                }

                return names;
            }
            else
            {
                College college = null;
                foreach (var item in _context.Colleges.Local.ToList())
                {
                    if (item.Name == name)
                    {
                        college = item;
                        break;
                    }
                }

                if (college == null)
                    return new List<string> { "Error: College '" + name + "' Not Found! Please check the spelling and try again." };

                return new List<string> { string.Format("{0:C}", GetTuition(college, roomAndBoard)) };
            }
        }

        List<College> BuildCollegesList()
        {
            List<College> collegeList = new List<College>();
            string csvFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Colleges.csv");
            using (var reader = new StreamReader(csvFilePath))
            {
                bool skipHeader = true;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    College college = new College(values[0], values[1], values[2], values[3]);
                    if (skipHeader)
                    {
                        skipHeader = false;
                        continue;
                    }
                    else
                        collegeList.Add(college);
                }
            }
            return collegeList;
        }

        Decimal GetTuition(College college, bool includeTuition)
        {
            Decimal tuition = Decimal.Parse(college.TuitionInState);
            if (includeTuition)
            {                
                Decimal roomBoard = Decimal.Parse(college.RoomBoard);
                return tuition + roomBoard;
            }
            else
            {
                return tuition;
            }
        }
    }
}
