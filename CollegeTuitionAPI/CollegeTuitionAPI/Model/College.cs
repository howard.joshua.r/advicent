﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollegeTuitionAPI.Model
{
    public class College
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string TuitionInState { get; set; }
        public string TuitionOutOfState { get; set; }
        public string RoomBoard { get; set; }

        public College(string name, string tuitionInState, string tuitionOutOfState, string roomBoard)
        {
            Name = name;
            TuitionInState = tuitionInState;
            TuitionOutOfState = tuitionOutOfState;
            RoomBoard = roomBoard;
        }

        public College() { }
    }
}
